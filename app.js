//importing
const express = require("express");
const mongoose = require("mongoose");
const VideoModel = require("./models/VideoModel");
const cors = require("cors");
require("dotenv").config();
const apiRouter = require("./routes/api");
const apiResponse = require("./helpers/apiResponse");

 
//app config
 const app = express();
 const port = process.env.PORT || 9000;
 
//middleware
app.use(express.json());
app.use(cors());

//db connection
const connection_url = "Enter Your URL";
mongoose.connect(connection_url, {
    useCreateIndex : true,
    useNewUrlParser : true,
    useUnifiedTopology : true
})

const db = mongoose.connection

db.once('open', ()=>{
    console.log("DB connected");
});

//Route Prefixes
//app.use("/", indexRouter);
app.use ("/api/", apiRouter);

//routes
app.get('/', (req,res) => res.status(200).send('Hey Jevesh'));

// throw 404 if URL not found

app.all("*", function(req, res) {
	return apiResponse.notFoundResponse(res, "Page not found");
});

//listen
app.listen(port, () => console.log('Listening to port: '+port)  );