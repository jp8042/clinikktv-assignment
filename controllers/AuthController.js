const User = require("../models/UserModel");
const { body,validationResult } = require("express-validator");
//helper file to prepare responses.
const apiResponse = require("../helpers/apiResponse");
const utility = require("../helpers/utility");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

/**
 * User registration.
 *
 * @param {string}      firstName
 * @param {string}      lastName
 * @param {string}      email
 * @param {string}      password
 *
 * @returns {Object}
 */
exports.register = [
	(req, res) => {	
		//hash input password
		User.findOne({email : req.body.email}).then((user) => {
		if (user) {
			return apiResponse.ErrorResponse(res,"Email already exists.");
		}
		else{
			bcrypt.hash(req.body.password,10,function(err, hash) {
			// Create User object with escaped and trimmed data
				var user = new User(
				{
					firstName: req.body.firstName,
					lastName: req.body.lastName,
					email: req.body.email,
					password: hash,
				}
			);
			// Save user.
			User.create(user,function (err, data) {
				if (err) { return apiResponse.ErrorResponse(res, err); }
					return apiResponse.successResponseWithData(res,"Registration Success.", data);
				});
			});
		}
	})}];

/**
 * User login.
 *
 * @param {string}      email
 * @param {string}      password
 *
 * @returns {Object}
 */
exports.login = [
	(req, res) => {
		try {
			User.findOne({email : req.body.email}).then(user => {
				if (user) {
					//Compare given password with db's hash.
					bcrypt.compare(req.body.password,user.password,
						function (err,same) {
						if(same){
							//Check account confirmation.
							let userData = {
								_id: user._id,
								firstName: user.firstName,
								lastName: user.lastName,
								email: user.email,
							};
							//Prepare JWT token for authentication
							const jwtPayload = userData;
							const jwtData = {
							expiresIn: process.env.JWT_TIMEOUT_DURATION,
							};
							const secret = process.env.JWT_SECRET;
							//Generated JWT token with Payload and secret.
							userData.token = jwt.sign(jwtPayload, secret, jwtData);
							return apiResponse.successResponseWithData(res,"Login Success.", userData);
						}else{
							return apiResponse.unauthorizedResponse(res, "Email or Password wrong.");
						}
					});
				}else{
					return apiResponse.unauthorizedResponse(res, "Email or Password wrong.");
				}
			});
		}
	 catch (err) {
		return apiResponse.ErrorResponse(res, err);
	}
}];

