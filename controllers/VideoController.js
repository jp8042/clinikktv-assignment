const utility = require("../helpers/utility");
const Video = require("../models/VideoModel");
const apiResponse = require("../helpers/apiResponse");
const auth = require("../middlewares/jwt");
const mongoose = require("mongoose");
mongoose.set("useFindAndModify", false);

// const fs = require('fs')
// const path = require('path')


function VideoData(data) {
	this.id = data._id;
	this.title= data.title;
	this.description = data.description;
    this.url = data.url;
    this.thumbnail = data.thumbnail;
    this.owner = data.owner;
	this.createdAt = data.createdAt;
}


exports.videoList = [
	auth,
	function (req, res) {
		try {
			Video.find().then((videos)=>{
				if(videos.length > 0){
					return apiResponse.successResponseWithData(res, "Operation success", videos);
				}else{
					return apiResponse.successResponseWithData(res, "Operation success", []);
				}
			});
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];
/*
    <video id="videoPlayer" controls muted="muted" autoplay> 
      <source src="/video/:id" type="video/mp4">
    </video>
*/
exports.videoPlay = [
    auth,
	function(req, res) {
		console.log("Logic Build in Process");
        // const video_id = req.params.id;
        // const path = `../assets/${video_id}.mp4`;
        // const stat = fs.statSync(path)
        // const fileSize = stat.size
        // const range = req.headers.range
      
        // if (range) {
        //   const parts = range.replace(/bytes=/, "").split("-")
        //   const start = parseInt(parts[0], 10)
        //   const end = parts[1]
        //     ? parseInt(parts[1], 10)
        //     : fileSize-1
      
        //   if(start >= fileSize) {
        //     res.status(416).send('Requested range not satisfiable\n'+start+' >= '+fileSize);
        //     return
        //   }
          
        //   const chunksize = (end-start)+1
        //   const file = fs.createReadStream(path, {start, end})
        //   const head = {
        //     'Content-Range': `bytes ${start}-${end}/${fileSize}`,
        //     'Accept-Ranges': 'bytes',
        //     'Content-Length': chunksize,
        //     'Content-Type': 'video/mp4',
        //   }
      
        //   res.writeHead(206, head)
        //   file.pipe(res)
        // } else {
        //   const head = {
        //     'Content-Length': fileSize,
        //     'Content-Type': 'video/mp4',
        //   }
        //   res.writeHead(200, head)
        //   fs.createReadStream(path).pipe(res)
        // }
      }      
]

exports.videoDetail = [
	auth,
	function (req, res) {
		if(!mongoose.Types.ObjectId.isValid(req.params.id)){
			return apiResponse.successResponseWithData(res, "Operation success", {});
		}
		try {
			Video.findOne({_id: req.params.id},"_id title description url ").then((video)=>{                
				if(video !== null){
					let videoData = new VideoData(video);
					return apiResponse.successResponseWithData(res, "Operation success", videoData);
				}else{
					return apiResponse.successResponseWithData(res, "Operation success", {});
				}
			});
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];


/**
 * Video store.
 * 
 * @param {string}      title 
 * @param {string}      description
 * @param {string}      url
 * 
 * @returns {Object}
 */
exports.videoStore = [
	auth,
	(req, res) => {
	    //const dbVideo = req.body;
		const dbVideo = new Video(
		{ 
			title: req.body.title,
			description: req.body.description,
			url: req.body.url
		});
		//Save video.
		Video.create(dbVideo, (err, data)=>{
			if(err){
				apiResponse.ErrorResponse(res, err);
			}
			else {
				apiResponse.successResponseWithData(res,"Video add Success.", data);
			}
		})
	}
];

/**
 * Video update.
 * 
 * @param {string}      title 
 * @param {string}      description
 * @param {string}      url
 * 
 * @returns {Object}
 */   
exports.videoUpdate = [
	auth,
	(req, res) => {
		try {
			const errors = validationResult(req);
			var video = new Video(
				{ title: req.body.title,
					description: req.body.description,
					url: req.body.url,
					_id:req.params.id
				});

			if (!errors.isEmpty()) {
				return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
			}
			else {
				if(!mongoose.Types.ObjectId.isValid(req.params.id)){
					return apiResponse.validationErrorWithData(res, "Invalid Error.", "Invalid ID");
				}else{
					Video.findById(req.params.id, function (err, foundVideo) {
						if(foundVideo === null){
							return apiResponse.notFoundResponse(res,"Video not exists with this id");
						}else{
							//Check authorized user
							if(foundVideo.user.toString() !== req.user._id){
								return apiResponse.unauthorizedResponse(res, "You are not authorized to do this operation.");
							}else{
								//update video.
								Video.findByIdAndUpdate(req.params.id, video, {},function (err) {
									if (err) { 
										return apiResponse.ErrorResponse(res, err); 
									}else{
										let videoData = new VideoData(video);
										return apiResponse.successResponseWithData(res,"Video update Success.", videoData);
									}
								});
							}
						}
					});
				}
			}
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Video Delete.
 * 
 * @param {string}      id
 * 
 * @returns {Object}
 */
exports.videoDelete = [
	auth,
	function (req, res) {
		if(!mongoose.Types.ObjectId.isValid(req.params.id)){
			return apiResponse.validationErrorWithData(res, "Invalid Error.", "Invalid ID");
		}
		try {
			Video.findById(req.params.id, function (err, foundVideo) {
				if(foundVideo === null){
					return apiResponse.notFoundResponse(res,"Video not exists with this id");
				}else{
					//Check authorized user
					if(foundVideo.user.toString() !== req.user._id){
						return apiResponse.unauthorizedResponse(res, "You are not authorized to do this operation.");
					}else{
						//delete video.
						Video.findByIdAndRemove(req.params.id,function (err) {
							if (err) { 
								return apiResponse.ErrorResponse(res, err); 
							}else{
								return apiResponse.successResponse(res,"Video delete Success.");
							}
						});
					}
				}
			});
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

