var express = require("express");
var authRouter = require("./auth");
var videoRouter = require("./video");

var app = express();

app.use("/auth/", authRouter);
app.use("/video/", videoRouter);

module.exports = app;