var express = require("express");
const VideoController = require("../controllers/VideoController");

var router = express.Router();

router.get("/", VideoController.videoList);
router.get("/:id", VideoController.videoDetail);
router.get("/play/:id", VideoController.videoPlay);
router.post("/store", VideoController.videoStore);
router.put("/:id", VideoController.videoUpdate);
router.delete("/:id", VideoController.videoDelete);

module.exports = router;