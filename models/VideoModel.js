const mongoose = require("mongoose");

const VideoSchema = mongoose.Schema({
	title: {type: String, required: true},
    description: {type: String, required: true},
    url: {type: String, required: true},
    thumbnail: {type: String},
}, {timestamps : true});

module.exports = mongoose.model("videoContents", VideoSchema);