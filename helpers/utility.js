const apiResponse = require('./apiResponse');

exports.randomNumber = function (length) {
	var text = "";
	var possible = "123456789";
	for (var i = 0; i < length; i++) {
		var sup = Math.floor(Math.random() * possible.length);
		text += i > 0 && sup == i ? "0" : possible.charAt(sup);
	}
	return Number(text);
};

exports.isValid = (req, res, next)=>{
	if(req.subscribed)
		next();
	apiResponse.unauthorizedResponse(res, "unauth");
}

exports.isAdmin = (req, res, next)=>{
	if(req.admin)
		next();
	apiResponse.unauthorizedResponse(res,"no admin privlage")
}